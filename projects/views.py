from django.shortcuts import get_object_or_404, redirect, render
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def projects_list(request):
    context = {"pro_list": Project.objects.filter(owner=request.user)}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    context = {"project": get_object_or_404(Project, id=id)}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
